# working with helm and chart

helm allows for easy deployment of complex configuration. This could be handy for a vendor to deploy a multi-partapplication in a single step. Local agents likeTilleruse the API to create objects on your behalf. Effectively its orchestration fororchestration.

there are a few ways to install helm. the newest version may require building from source code. in this exercise we will install from source code

1. download helm package, extract and copy to binary files
```bash
wget get https://bit.ly/2IIn5W
tar -xzf 2IIn5W
sudo cp linux-amd64/helm /usr/local/bin/
```
2. Due to new RBACconfigurationhelmis unable to run in thedefaultnamespace, in this version of Kubernetes. we will craete service account for tiller and give it admin abilites on the cluster.
```bash
# begin by creating serviceaccount object
kubectl create serviceaccount --namespace=kube-system tiller
#bind the serviceaccount to the admin role called cluster-admin inside kube-system namespace
kubectl create clusterrolebinding tiller-culster-rule --clusterrole=clusteradmin --serviceaccount=kube-system:tiller
```
3. after that we can initialize helm. this process will also configure tiller the client process.
```bash
helm init
```
4. update tiller-deploy deployment to have the service account
```bash
kubectl -n kube-system patch deployment tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
```
5. verify the tiller pod is running
```bash
kubectl get pods --all-namespaces
```
6. verify helm version
```bash
helm version
```
7. ensure both are upgraded to the most recent stable version.
```bash
helm init --upgrade
```
8. chart is collection of package or deployment application the collection are in this github link https://github.com/kubernetes/charts/tree/master/stable. we can search chart using helm command
```bash
helm search database
```
9. in this exercise we will install mariadb from helm .
```bash
helm --debug install stable/mariadb \--set master.persistence.enabled=false \--set slave.persistence.enabled=false
# debug command will create a lot of output
```
10. using some information in the end of output we can search mysql passwor.
```bash
kubectl get secret --namespace default flailing-peahen-mariadb -o jsonpath="{.data.mariadb-root-password}" | base64 --decode
```
11. now we will install another container to act as client for database . we will use apt-get to install client software.
```bash
kubectl run -i --tty ubuntu --image=ubuntu:16.04 --restart=Never -- bash -il
```
12. inside container install mariadb client and then access mysql
```bash
apt-ge update  ; apt-get install -y mariadb-client
mysql -h illmannered-salamander-mariadb -p
```
13. we can add another repository and then search chart from that repository
```bash
helm repo add common \http://storage.googleapis.com/kubernetes-chart
helm repo list
helm search | less
```

