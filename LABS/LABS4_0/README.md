# Limit resources on kubernetes
we can limit our resource in kubernetes with 2 ways.
    
1. using *** specs: *** on yaml configuration file
2. using namespace defenition.
    
## 1. using *** spec: *** on yaml configuration file
1. create example deployment from image
    ``` bash
    kubectl create deployment hog --image vish/stress
    ```
2. get yaml file from describe command and delete unnecessary line uid, selflink, status, timecreate
    ```bash
    kubectl describe deployment hog -o yaml > hog.yaml
    ```
3. add this line in yaml configuration file to limit resource.
    ```yaml
    spec:
      containers:
      - image: vish/stress
        imagePullPolicy: Always
        name: stress
        resources:
          limits:
            memory: "4Gi"
          requests:
    ```
4. apply new configuartion 
    ```bash
    kubectl create -f hog.yaml
    ```
5. to verify the configuration
    ```bash
    kubectl get deployment hog -o yaml | grep cpu
    ```

##  2. using namespace defenition.
with namespace we can create virtual cluster in our physical cluster, it will help to prevent potential collision name for between team, provision our cluster.

1. list our namespace
    ```bash
    kubectl get namespace
    ```
2. create namespace
    ```bash
    kubectl create namespace low-usage-limit
    ```
3. create yaml file to limit our namespace
    ```yaml
    apiVersion: v1
    kind: LimitRange
    metadata:
    name: low-resource-range
    spec:
    limits:
    - default:
        cpu: 1
        memory: 500Mi
        defaultRequest:
        cpu: 0.5
        memory: 100Mi
        type: Container
    ```
4. apply those yaml to our namespace
    ```bash
    kubectl --namespace=low-usage-limit create -f low-resource-range.yaml 
    ```
5. create new deployment on our namespace
    ```bash
    kubectl create deployment hog --image vish/stress -n low-resource-range
    ```
6. verifiy our configuration
    ```bash
    kubectl get deployment hog -o yaml | grep cpu
    ```
