# introduction JOB kubernete
While most API objects are deployed such that they continue to be available there are some which we may want to run aparticular number of times called a Job

#CronJob
A CronJobcreates a watch loop which will create a batch job on your behalf when the time becomes true

this is an example cronjob that will active every 2 minutes.
1. create file yaml cronjob.yaml
```yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: sleepy
spec:
  schedule: "*/2 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          activeDeadlineSeconds: 10
          containers:
            - name: resting
              image: busybox
              command: ["/bin/sleep"]
              args: ["5"]
          restartPolicy: Never
```
2. apply yaml
```bash
kubectl create -f cronjob.yaml
```
3. verification using this command
```bash
kubectl get cronjobs sleepy
```
4. check your job using this command
```bash
kubectl get jobs
```

