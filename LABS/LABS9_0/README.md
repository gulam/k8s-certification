
## 2. creating persistent NFS volume

1. install nfs server on master kubernetes
```bash
sudo apt-get update && sudo \apt-get install -y nfs-kernel-server
```
2. make and populate directory to be shared
```bash
sudo mkdir /opt/sfw
sudo chmod 777 /opt/sfw
sudo echo software > /opt/sfw/hello.text
```
3. edit nfs server file to share out the newly craeted directory.
```bash
vim /etc/exports
/opt/sfw/ *(rw,sync,no_root_squash,subtree_check)
```
4. reread /etc/exports
```bash
sudo exportfs -ra
```
3. go to second node or worker and then mount nfs.
```bash
sudo apt-get -y install nfs-common
showmount -e server01 # <-- server01 hostname master kubernetes
```
4. mount partition nfs
```bash
mount server01:/opt/sfw /mnt
```
5. return to master node and create persistent volume
```yaml
piVersion: v1
kind: PersistentVolume
metadata:
  name: pvvol-1
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  nfs:
    path: /opt/sfw
    server: server01
    readOnly: false
```
kubectl create -f PVol1.yaml
6. verify persistent volume
```bash
kubectl get pv
```



## 3. Creating PVC (Persistent Volume Claim)
before pod get volume from PV we need to create pvc first.

1. check your pvc is exist
```bash
kubectl get pvc
```
2. create yaml file to create pvc
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-one
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: 200Mi
```
3. apply pvc
```bash
kubectl create -f pvc-one.yaml
```
4. look the status of pvc, it should show a status bound.
```bash
kubectl get pvc
```
5. create a new deployment to use pvc.
```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  annotations:
    deployment.kubernetes.io/revision: "1"
  generation: 1
  labels:
    app: nginx
  name: pod-aing 
  namespace: default
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: nginx
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx
        imagePullPolicy: Always
        name: nginx
        ports:
        - containerPort: 80
          protocol: TCP
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      volumes:
        - name: nfs-vol
          persistentVolumeClaim:
            claimName: pvc-one
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
```

6. apply new deployment
```bash
kubectl create -f nfs-pod.yaml
kubectl get pod
```
7. check status pvc again, it should a status bound
```bash
kubectl get pvc
```

### 4. using resourcequota to limit quota pvc count and usage 
we can limit consumption usage among user and limit the number persisten volume claim.




