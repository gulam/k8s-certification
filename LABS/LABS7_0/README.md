# Introduction
In this lab we will first explore the APIobjects used to manage groups of containers. The objects available have changed as Kubernetes has matured, so theKubernetes version in use will determine which are available.  Our first object will be aReplicaSet, which does notinclude newer management features found withDeployments.  ADeploymentwill will manageReplicaSetsfor you.We will also work with another object called aDaemonSetwhich ensures a container is running on newly added node.

## Replica Set : lab 7.1
1. create replicaset yaml like this.
```yaml
apiVersion: extensions/v1beta1 
kind: ReplicaSet
metadata:
  name: rs-one
spec:
  replicas: 2
  template: 
    metadata:
      labels:
        system: ReplicaOne
    spec:
      containers:
        - name: nginx-rs
          image: nginx:1.9.1
          ports: 
          - containerPort: 80
```
2. apply replica set
```bash
kubectl create -f rs.yaml
```
3. verification
```bash
kubectl get rs
kubectl get pods
```
4. delete repilcaset, but now the pod it control.
```bash
kubectl delete rs rs-one --cascade=false
```
5. the replicaset will delted but pod it controlled will not delted
```bash
kubectl get rs
kubectl get pod
```
6. create replicaset again
```bash
kubectl create -f rs.yaml
```
7. try to isolate pod from replica set using label, the replica set will craete another pod based on configuration spesification
```bash
kubectl edit po rs-one-xxx 
labels:
  system: isolatedpod #<-- change from ReplicaOne
name: rs-one-xxx
```
8. show pods, the number of pod now will be 3.
```bash
kubectl get pod -L system
```
9. show the replicaset,the replica set always create 2 pod.
```bash
kubectl get rs 
```

## DaemonSets: Lab 7.2
Deamonset is watch loop like deployment. the daemonset ensures that when a node is added to a cluster a pod will be created on that node.

Deployment would only ensure a particular number of pod are craeted in general, several could be in single node. using DaemonSets can be helpful to ensure applications are one each node. when node is removed DaemonSets will ensure that pod will also removed.


1. create yaml file like this:
```yaml
kind: DaemonSet
metadata:
  name: ds-one
spec:
  template: 
    metadata:
      labels:
        system: DaemonSetOne
    spec:
      containers:
        - name: nginx-rs
          image: nginx:1.9.1
          ports: 
          - containerPort: 80
```
2. apply to kubernetes cluter
```bash
kubectl create -f ds.yaml
```
3. verify daemonset
```bash
kubectl get ds
kubectl get pods
```
4. verify that pod distribute to every node
```bash
kubectl describe pods ds-one-xx | grep -i node
```

## Rolling Updates and Rollback : Lab 7.3
there are 2 type for rolling update sytem
1. OnDelete: when we finshed edit daemonset template, daemonset old pod will remain until we remove pod deamonset manualy.
2. RollingUpdate: (default Strategy) when we finished edit daemonset template, deamonset will automatically kill old pod and then create the new one.

### rolling update uinsg OnDelete Strategy.
1. check rolling update stragegy daemonset:
```bash
kubectl describe ds ds-one | grep -A1 -i strategy
```
2. if we are using kuberntes 1.14 default strategy is OnDelete, try to edit image existing daemonset.
```bash
kubectl edit ds ds-one

change image from nginx.1.19.1 to 1.12.1-alpine
```
3. verify existing daemonset pod using this command
```bash
kubectl describe pod ds-one-xxx | grep Image:
```
4. delete existing daemonset pod
```bash
kubectl delete pod ds-one-xxx
```
5. verify pod
```bash
kubectl get pod
kubectl get pod ds-one-yyy | grep Image:
# kubernetes will create new pod with image  1.12.1-alpine
```
6. show history rolling update
```bash
kubectl rollout history daemonset ds-one
# it will shown 2 revision, try to desribe every version of rollout.
kubectl rolllout history daemonset ds-one --revision=2
```
7. we can rollback to version 1 using this command
```bash
kubectl rollout undo ds ds-one --to-revision=1
```

### rolling update using RollingUpdate strategy.

1. copy ds-one template 
```bash
kubectl describe ds ds-pne -o yaml --export > ds-two.yaml
```
2. edit ds-two.yaml
```bash
vim ds-two.yaml
#edit strategy update to RollingUpdate
  updateStrategy:
    rollingUpdate:
      maxUnavailable: 1
    type: RollingUpdate <-- change to this
```
3. apply to kubernetes
```bash
kubectl create -f ds-two.yaml
```
4. try to update image with the new one.
5. verify pod is updated with this command
```bash
kubectl get pod
kubectl desctibe pods ds-two-xxx | grep -i image
```

