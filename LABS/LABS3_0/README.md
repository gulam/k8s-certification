# Lab 3.1 Installing kubernetes on ubuntu 16.04 

1. update repository
    ```bash
    apt-get update && apt-get upgrade -y 
    ```
2. chose dockers as container for our infrastructure, we can also use cri-io but it require extra work to enable kubernetes
    ```bash
    apt-get install docker.io -y
    ```
3. add kubernetes repository and add gpg key
    ```bash
    vim /etc/apt/sources.list.d/kubernetes.list
    deb http://apt.kubernetes.io/ kubernetes-xenial main
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    ```
4. update repository
    ```bash
    apt-get update 
    ```
5. install kubernetes packages
    ```bash
    apt-get install kubeadm=1.14.1-00 kubectl=1.14.1-00 kubelet=1.14.1-00 -y
    ```
6. create CNI ( container network interface) we will use calico as CNI, first download 
    ```bash
    wget https://tinyurl.com/yb4xturm -O rbac-kdd.yaml
    wget https://tinyurl.com/y8lvqc9g -O calico.yaml
    ```
7. check pods network from calico.yaml, it will use for initialize kubernetes cluster. find line CALICO_IPV4POOL_CIDR
    
    "value:"192.168.0.0/16"
8. initialize master with this command
    ```bash
    kubeadm init \ 
    --kubernetes-version 1.14.1 \
    --pod-network-cidr 192.168.0.0/16 | tee kubeadm-init.out
    ```
9. from initialize output command,we will a non-root user to access cluster
    ```bash
    exit
    $ mkdir -p $HOME/.kube
    $sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    $sudo chown $(id -u):$(id -g) $HOME/.kube/config
    ```
10. apply network configuration to our cluster
    ```bash
    $ sudo cp /root/rbac-kdd.yaml .
    $ sudo cp /root/calico.yaml .
    $ kubectl apply -f rbac-kdd.yaml
    $ kubectl apply -f calico.yaml
    ```
11. because kubectl have many command to use, we need bash auto-completion
    ```bash
    $ source <(kubectl completion bash)
    $ echo "source <(kubectl completion bash)" >> ~/.bashrc
    ```
# Lab 3.2 Grow Cluster
1. update repository
    ```bash
    apt-get update && apt-get upgrade -y
    ```
2. chose dockers as container for our infrastructure, we can also use cri-io but it require extra work to enable kubernetes
    ```bash
    apt-get install docker.io -y
    ```
3. add kubernetes repository and add gpg key
    ```bash
    vim /etc/apt/sources.list.d/kubernetes.list
    deb http://apt.kubernetes.io/ kubernetes-xenial main
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    ```
4. update repository
    ```bash
    apt-get update 
    ```
5. install kubernetes packages
    ```bash
    apt-get install kubeadm=1.14.1-00 kubectl=1.14.1-00 kubelet=1.14.1-00 -y
    ```
## login into master
1. get token information, this token only valid 24 hours if we want to generate token use kebadm token create
    ```bash
    kubeadm token list
    ```
2. start from 1.9 hould create and use a Discovery Token CA Cert Hash created from the master to ensure the nodejoins the cluster in a secure manner.  
    ```bash
    openssl x509 -pubkey \-in /etc/kubernetes/pki/ca.crt | openssl rsa \-pubin -outform der 2>/dev/null | openssl dgst \-sha256 -hex | sed 's/^.* //'
    ```
    **copy output hash code**
   
## login into worker/node
1. join the worker/node to master using token code and hash code
    ```bash
    kubeadm join --token dm1iwy.3jj7up73ziyeym59 167.205.1.231:6443 --discovery-token-ca-cert-hash sha256:b4dc62ffcc07bdbcaa2bb7657f53e9f097616141808545cc18c828e23045ae97
    ```

2. if join successfuly you wil get output 
    
This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

# Lab 3.3 Finish Cluster Setup
1. check node cluster on master server
    ```bash
    kubectl get nodes
    ```
2. check pods for dns and networking calico is working
    ```bash
    kubectl get pods --all-namespaces
    ```
    
 if calico or dns is stuck in creating container try remove pods using this command
       *kubectl -n kube-system delete pod coredns-xxx*
# Lab 3.4 Deploy A simple application

to change configuration in cluster can be done with subcommand apply,edit or patch for non-disurptive updates. 
the apply command does three-way diff previous, current and supplied input to determine modifications to make.
if the configuration has resource field which cannto be updated once initialized then a disruptive update could be done using the replace --force option.

1. create simple application 
    ```bash
    $ kubectl create deployment nginx --image=nginx
    ```
2. get information about deployment
    ```bash
    $ kubectl get deployment nginx
    $ kubectl describe deployment nginx
    ```
3. get information about deployment with yaml format
    ```bash
    $ kubectl describe deployment nginx -o yaml > first.yaml
    ```
4. delete line "resourceversion, selflink,uid and all status tab"
    ```bash
    $ vim first.yaml
    ```
5. delete  nginx deployment and start new deployment with yaml file
    ```bash
    $ kubectl delete deployment nginx
    $ kubectl create -f first.yaml
    ```
6. edit container to expose port 80
    ```bash
    $ vim first.yaml
    ```
    **add container port inside container spec configuration**
    `ports:
    - containerPort: 80
      protocol: TCP`
7. recreate deployment again
    ```bash
    $ kubectl replace -f first.yaml
    ```
8. deployment
    ```bash
    $ kubectl expose deployment nginx --type=NodePort
    ```
9. scale deployment nginx
    ```bash
    $ kubectl scale deployment nginx --replicas=3
    ```
10. try access webserver using cluster ip address
    ```bash
    $ kubectl get svc
    $ curl 10.98.190.170:80
    ```
additinal command info
check endoint ip address
```bash
kubectl get ep nginx
```
get detail information about pods running on
```bash
kubectl get pods -o wide
```
if we are using kubeadm or private server for expose service we can only use NodePort type and Ingress Controller (Integrate F5 or NGINX)
