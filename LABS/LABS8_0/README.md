# LAB SERVICES
## 1. In this exercies we deployed endpoint service.

1. create yaml file like this and then apply to cluster
```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: nginx-one
  labels:
    system: secondary
  namespace: accounting
spec:
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx:1.9.1
        imagePullPolicy: Always
        name: nginx
        ports:
        - containerPort: 80
          protocol: TCP
      nodeSelector:
        system: secondOne
```
2. from the above yaml command we need to create label on worker node with labal seconOne
```bash
kubectl label node server02 sytem=SecondOne
```
3. create namespace accounting 
```bash
kubectl create ns accounting
```
4. show label from node
``bash
kubectl get node --show-labels
```
5. apply yaml file to cluster
```bash
kubectl create -f nginx-one.yaml
```
6. show pods
```bash
kubectl get pods -n accounting
```
7. expose deployment nginx-one with endpoint service.
```bash
kubectl -n accounting expose deployment nginx-one 
```
8. view the newly endpoint, note that 8080 is exposed on each pod.
```bash
kubectl -n account get endpoint nginx-one
```
9. attempt to access pod on port 8080 then port 80. Even though we exposed port 8080 of the container theapplication within has not been configured to listen on this port.  Thenginxserver will listens on port 80 by default.  Acurlcommand to that port should return the typical welcome page.
```bash
curl 192.168.1.67:8080
curl 192.168.1.67:80
```

## 2. NodePort Services

1. using the same yaml file we will expose our nginx to outside cluster
```bash
kubectl -n accounting expose nginx-one --type=NodePort
```
2. check your cluster ip address
```bash
kubectl cluster-info
```
3. get port ip addres, so we can access service nginx with cluster ip and port
```bash
kubectl -n accounting get svc 
   # NodePort:                 <unset>  30529/TCP
```
4. try to access service nginx with cluster ip address and port
```bash
curl 167.205.1.231:30529
``` 

## use label to mange resource.
1. show label on pod
```bash
kubectl -n accounting get pods --show-label
```
2. delete pods based on label
```bash
kubectl -n accounting delete pods -l apps=nginx
```
3. delete deployment using label
```bash
kubectl -n accounting delete deploymeng -l system=secondary
```
