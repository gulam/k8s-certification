# Creating Ingress Controller
 If you have a large number of services to expose outside of the cluster, or to expose a low-number port on the host nodeyou can deploy an ingress controller or a service mesh.  In this exercise we will install traefik ingress controller.

1. create 2 deployment nginx to test ingress controller
```bash
kubectl create deployment secondapp --image=nginx
kubectl create deployment thirdapp --image=nginx
```
2. expose all those deployment
```bash
kubectl expose deployment secondapp --type=NodePort --port=80
kubectl expose deployment thirdpage --type=NodePort --port=80
```
3. as we have RBAC configure we need to make sure the controller wil run and be abe to work with all necessary ports. craete yaml file to create RBAC.
```yaml
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: traefik-ingress-controller
rules:
  - apiGroups:
      - ""
    resources:
      - services
      - endpoints
      - secrets
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - extensions
    resources:
      - ingresses
    verbs:
      - get
      - list
      - watch
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: traefik-ingress-controller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: traefik-ingress-controller
subjects:
- kind: ServiceAccount
  name: traefik-ingress-controller
  namespace: kube-system
```
```bash
kubectl create -f ingress.rbac.yaml
```
4. create traefik controller, download yaml file first.
```bash
wget https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/traefik-ds.yaml
```
deploy traefik controller
```bash
kubectl craete -f traefik.yaml
```
5. after we finished install ingress controller the next thing to do is create rule for ingress. create this yaml file.
```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: traefik
  generation: 3
  name: ingress-test
spec:
  rules:
  - host: www.example.com
    http:
      paths:
      - backend:
          serviceName: secondapp # <-- service name must match from this command , "kubectl get svc"
          servicePort: 80
        path: /
  - host: www.thirdpage.com
    http:
      paths:
      - backend:
          serviceName: thirdpage  # <-- service name must match from this command , "kubectl get svc"
          servicePort: 80
        path: /
status:
  loadBalancer: {}
```

6. testing our ingress controller using curl command
```bash
curl -H "Host:www.thirdpage.com" http://167.205.1.231/
curl -H "Host:www.example.com" http://167.205.1.231/
curl -H "Host:www.thirdpage.com" http://10.100.81.37/
```
from the above command we can access our service from cluster ip or nodeport ip.


