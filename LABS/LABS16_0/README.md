# overview
setiap cluster dapat menyimpan cluster root certificate authorities (CA) secara default menggunakan CAnya sendiri digunakan untuk komunikasi antara intracluster.
CA certificate bundle didistribusikan ke setiap node dan digunakan sebagai secret untuk service accountnya. kubelet adalah local agent yang menjamin local container tetap stabil.

## authentication and authorization
kubernetes have 2 type of users service account and normal users.
normal users are assumed to be managed by outside service, There are no objects to represent them and they cannot be added via an APIcall, butservice accountscan be added.

we will use RBAC to configure to action with a namespace for new contractor, developer Asep  who will be working on a new project

1. create user account DevAsep and namespace development
```bash
sudo useradd -s /bin/bash DevAsep
sudo passwd DevAsep
kubectl create namespace development
```
2. create tls certificate for new user
```bash
openssl genrsa -out DevAsep.key 2048
openssl req -new -key DevAsep.key -out DevAsep.csr -subj "/CN=DevAsep/O=development"
# set activation user only 45 days
sudo openssl x509 -req -in DevAsep.csr -CA /etc/kubernetes/pki/ca.crt -CAkey /etc/kubernetes/pki/ca.key -CAcreateserial -out DevAsep.crt -days 45
```
3. add new user with certificate to kubernetes cluster
```bash
kubectl config set-credentials DevAsep --client-certificate=/home/ghulam/DevAsep.crt --client-key=/home/ghulam/DevAsep.key 
```
4. create new context for Dev Asep and then assign to namespace development
```bash
kubectl config set-context DevAsep-context --cluster=kubernetes --namespace=development --user=DevAsep
```
5. try to access pod with context DevAsep-context and namespace development
```bash
kubectl --context=DevAsep-context get pods
# the output will account DevAsep not authorized to execute this command. because of that we need to craete role for user DevAsep and then binding that role using rolebinding to DevAsepuser
```
6. create file yaml contain role configuration
```yaml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: Role
metadata:
  namespace: development
  name: developer
rules:
- apiGroups: ["", "extensions", "apps"]
  resources: ["deployment", "replicasets", "pods"]
  verbs: ["list", "get", "watch", "create", "update", "patch", "delete"]
```
7. apply configuration to kubernetes
```bash
kubectl create -f role.yaml
```
8. create yaml file to binding role for user DevAsep
```yaml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata: 
  namespace: development 
  name: developer-role-binding
subjects:
- kind: User
  name: DevAsep
  apiGroup: ""
roleRef:
  kind: Role
  name: developer
  apiGroup: ""
```
9. apply yaml file to kubernetes
```bash
kubectl create -f rolebinding.yaml
```
10. now verify we can get access to list of pod 
```bash
kubectl --context=DevAsep-context get pods
```
11. try to create deployment using context DevAsep
```bash
kubectl --context=DevAsep-context create deployment nginx --image=nginx
kubectl --context=DevAsep-context get pods
```
12. the next scenario we will create new namespace production and then assign DevAsep role just read the production resources namespace
```bash
kubectl create namespace production
```
13. create role for production
```yaml
kind: Role
metadata:
  namespace: production
  name: role-production
rules:
- apiGroups: ["", "extensions", "apps"]
  resources: ["deployment", "replicasets", "pods"]
  verbs: ["list", "get", "watch"]
```
14. apply role
```bash
kubectl create -f role-prod.yaml
```
15. create rolebinding for production
```yaml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata: 
  namespace: production
  name: production-role-binding
subjects:
- kind: User
  name: DevAsep
  apiGroup: ""
roleRef:
  kind: Role
  name: role-production 
  apiGroup: ""
```
16. apply rolebinding
```bash
kubectl create -f rolebind-prod.yaml
```
17. verify user contxt DevAsep can only read production namespace
```bash
kubectl --context=DevAsep-context create deployment nginx --image=nginx
kubectl --context=DevAsep-context -n production get pods
```
