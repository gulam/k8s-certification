# Access kubernetes resource from curl
we can access kubernetes resources from curl command, kubernetes can only read json file but if we use kubectl, kubectl will convert yaml file to json file.

1. set client-certificate as variable
```bash
 export client=$(grep client-cert ~/.kube/config |cut -d" " -f 6)
```
2. show variable 
```bash
echo $client
```
3. set client-key, certificate authoriity as variable.
```bash
export key=$(grep client-key-data ~/.kube/config |cut -d " " -f 6)
export auth=$(grep certificate-authority-data ~/.kube/config |cut -d " " -f 6)
```
4. now encode file for use with curl
```bash
echo $client | base64 -d - > ./client.pem
echo $key | base64 -d - > ./client-key.pem
echo $auth | base64 -d - > ./ca.pem
```
5. try using curl command to get list of pod
```bash
curl --cert client.pem --key client-key.pem --cacert ca.pem https://167.205.1.231:6443/api/v1/pods
```
6. create example pods json file and then post to kubernetes cluters
```json
{
    "kind": "Pod",
    "apiVersion": "v1",
    "metadata":{
        "name": "curlpod",
        "namespace": "default",
        "labels": {"name": "examplepod"
        }
    },
    "spec": {
        "containers": [{
            "name": "nginx",
            "image": "nginx",
            "ports": [{"containerPort": 80}]
        }]
    }
}
```
7. post with curl command
```bash
url --cert client.pem --key client-key.pem --cacert ca.pem https://167.205.1.231:6443/api/v1/namespaces/default/pods -XPOST -H'Content-Type: application/json' -d@curlpod.json
```
8. verify with kubectl command
```bash
kubectl get pods
```